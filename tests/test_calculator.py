import random
import endpoints
import requests


def test_add_calculator():
    first_random_number = random.randint(1,10)
    second_random_number = random.randint(1,10)
    body = {
        "firstNumber": first_random_number,
        "secondNumber": second_random_number
}

    add_calculator_response = requests.post(endpoints.calculator_add, json=body)
    assert add_calculator_response.status_code == 200
    result = add_calculator_response.json()['result']   #wyciągnięcie kliucza ze słownika add_calculator_response.json()
    result_add = first_random_number + second_random_number
    assert result == result_add
