import requests
import endpoints

def test_send_ping():
    response = requests.get(endpoints.ping)
    assert response.status_code == 200
    assert response.text == 'pong'

#ping_response json - słownik - jest metodą
#status_code - status - pole klasy
#text - tekst -pole klasy
#headers - wyciaga nagłowki -pole klasy

def test_ping_as_json():
    headers_dict = {
        'Accept': 'application/json'
    }
    ping_response = requests.get(endpoints.ping, headers=headers_dict)
    assert ping_response.status_code == 200
    response_dict = ping_response.json()
    assert response_dict['reply'] == 'pong!'