import pytest
import requests
from faker import Faker
import endpoints
from models.Account import Account

fake = Faker()  # inicjujemy obiekt klasy Faker


def test_get_accounts_list(new_account):

    response = requests.get(endpoints.accounts)
    assert response.status_code == 200

    response_dict = response.json()
    print(response.json())  # metoda robi nam slownik pythonowy
    print(response_dict['accounts'])
    accounts_list = response_dict['accounts']
    names_list = [a['name'] for a in accounts_list]
    print(names_list)
    assert new_account.name in names_list

def test_check_ammount_balance(new_account):

    account_name_params = {'account': new_account.name}
    account_dict = requests.get(endpoints.accounts, params=account_name_params)
    check_balance = account_dict.json()['accounts'][0]['balance']['accountBalance']  ## zagniezdzanie w slowniku
    assert check_balance == 1000


def test_create_account(new_account):
    assert new_account.get_balance() == 1000



def test_delete_account(new_account):
    new_account.delete()

    account_name_params = {'account': new_account.name}  #--> usuwamy, bo zastepujemy metodą - obiekt
  #  response_delete_account = requests.delete(endpoints.accounts_delete, params=account_name_params)
  #  assert response_delete_account.status_code == 200
    response = requests.get(endpoints.accounts, params=account_name_params)
    assert response.status_code == 404

def test_case(new_account):
    new_account.pay(200)
    assert new_account.get_balance() == 1200
    new_account.withdraw(133)
    assert new_account.get_balance() == 1067



@pytest.fixture
def new_account():
    account = Account()
    account.create()
    return account


#@pytest.fixture
#def account_name():
 #   random_name = fake.uuid4()
  #  body = {
#     "name": random_name
# }
 #   create_account_response = requests.put(endpoints.accounts_create, json=body)#
 #   assert create_account_response.status_code == 201
#    return random_name